package com.maolei.mp3.activity;

import com.maolei.mp3.R;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public class MainActivity extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		TabHost tabHost = getTabHost();
		Intent remoteIntent = new Intent();
		remoteIntent.setClass(this, RemoteListActivity.class);
		TabHost.TabSpec remoteSpec = tabHost.newTabSpec("远程");
		Resources res = getResources();
		remoteSpec.setIndicator("远程", res.getDrawable(android.R.drawable.stat_sys_download));
		remoteSpec.setContent(remoteIntent);
		tabHost.addTab(remoteSpec);
	}

}
