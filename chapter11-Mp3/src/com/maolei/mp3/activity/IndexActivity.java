package com.maolei.mp3.activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.maolei.mp3.R;
import com.maolei.mp3.download.HttpDownloader;
import com.maolei.mp3.model.Mp3Info;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class IndexActivity extends Activity {

	private static final int UPDATE = 1;
	private static final int ABOUT = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, UPDATE, 1, R.string.app_name);
		menu.add(0, ABOUT, 2, R.string.app_name);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == UPDATE) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					String json = downloadJson("http://192.168.1.167:8080/mp3/res.json");
					try {
						JSONObject jsonObject = new JSONObject(json);
						JSONArray res = jsonObject.getJSONArray("res");
						for (int i = 0; i < res.length(); i++) {
							Mp3Info mp3info = new Mp3Info();
							// mp3info.to(res.getJSONObject(i));
							Log.d("JSON", mp3info.toString());
						}

					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			}).start();

		} else if (id == ABOUT) {

		}
		return super.onOptionsItemSelected(item);
	}

	private String downloadJson(String urlStr) {
		HttpDownloader httpDownloader = new HttpDownloader();
		String res = httpDownloader.download(urlStr);
		return res;
	}

}
