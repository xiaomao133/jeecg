package com.maolei.mp3.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.maolei.mp3.R;
import com.maolei.mp3.download.HttpDownloader;
import com.maolei.mp3.model.Mp3Info;
import com.maolei.mp3.service.DownloaderService;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class RemoteListActivity extends ListActivity {

	private List<Mp3Info> mp3Infos = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_remote);
		// 4.0以后下载东西必须在线程里运行，不然会报错
		new Thread(new Runnable() {

			@Override
			public void run() {
				updateListView();
			}

		}).start();

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// 得到Mp3Info
		Mp3Info mp3Info = mp3Infos.get(position);
		// 初始化Intent
		Intent intent = new Intent();
		// 把Mp3Info放到Intent中
		intent.putExtra("mp3Info", mp3Info);
		// 设置跳转的对象
		intent.setClass(this, DownloaderService.class);
		// 启动service
		startService(intent);
		super.onListItemClick(l, v, position, id);
	}

	/**
	 * 更新远程端列表数据
	 */
	private void updateListView() {
		// 下载服务器上的数据文件
		String json = downloadJson("http://192.168.1.167:8080/mp3/res.json");
		// 把文件内容转换成Mp3Info集合
		mp3Infos = parses(json);

		// 把Mp3Info集合转换成HashMap集合
		List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		for (Mp3Info mp3Info : mp3Infos) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("mp3_name", mp3Info.getMp3Name());
			map.put("mp3_size", mp3Info.getMp3Size().toString());
			list.add(map);
		}

		// 把数据传给列表
		SimpleAdapter simpleAdapter = new SimpleAdapter(RemoteListActivity.this, list, R.layout.mp3info_item,
				new String[] { "mp3_name", "mp3_size" }, new int[] { R.id.mp3_name, R.id.mp3_size });
		setListAdapter(simpleAdapter);
	}

	/**
	 * json格式转换成Mp3Info集合
	 * 
	 * @param json
	 * @return
	 */
	private List<Mp3Info> parses(String json) {
		List<Mp3Info> mp3Infos = new ArrayList<Mp3Info>();
		try {
			JSONArray jsonArray = new JSONArray(json);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Mp3Info mp3info = new Mp3Info(jsonObject);
				mp3Infos.add(mp3info);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return mp3Infos;
	}

	/**
	 * 下载文件读取文件内容
	 * 
	 * @param urlStr
	 * @return
	 */
	private String downloadJson(String urlStr) {
		HttpDownloader httpDownloader = new HttpDownloader();
		String res = httpDownloader.download(urlStr);
		return res;
	}
}
