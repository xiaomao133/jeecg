package com.maolei.mp3.model;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class Mp3Info implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;

	private String mp3Name;
	private Integer mp3Size;

	private String lrcName;
	private Integer lrcSize;

	public Mp3Info() {
	}

	public Mp3Info(JSONObject jsonObject) throws JSONException {
		this.id = jsonObject.getString("id");
		this.mp3Name = jsonObject.getString("mp3Name");
		this.mp3Size = jsonObject.getInt("mp3Size");
		this.lrcName = jsonObject.getString("lrcName");
		this.lrcSize = jsonObject.getInt("lrcSize");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMp3Name() {
		return mp3Name;
	}

	public void setMp3Name(String mp3Name) {
		this.mp3Name = mp3Name;
	}

	public Integer getMp3Size() {
		return mp3Size;
	}

	public void setMp3Size(Integer mp3Size) {
		this.mp3Size = mp3Size;
	}

	public String getLrcName() {
		return lrcName;
	}

	public void setLrcName(String lrcName) {
		this.lrcName = lrcName;
	}

	public Integer getLrcSize() {
		return lrcSize;
	}

	public void setLrcSize(Integer lrcSize) {
		this.lrcSize = lrcSize;
	}

	@Override
	public String toString() {
		return "Mp3Info{" + "id='" + id + '\'' + ", mp3Name='" + mp3Name + '\'' + ", mp3Size=" + mp3Size + ", lrcName='"
				+ lrcName + '\'' + ", lrcSize=" + lrcSize + '}';
	}
}
