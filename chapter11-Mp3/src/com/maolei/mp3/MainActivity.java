package com.maolei.mp3;

import com.maolei.mp3.activity.LocalListActivity;
import com.maolei.mp3.activity.RemoteListActivity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

@SuppressWarnings("deprecation")
public class MainActivity extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// 得到tabHost对象
		TabHost tabHost = getTabHost();

		// 生成一个Intent对象，该对象指向一个Activity
		Intent remoteIntent = new Intent();
		remoteIntent.setClass(this, RemoteListActivity.class);

		TabHost.TabSpec remoteSpec = tabHost.newTabSpec("远程");
		remoteSpec.setIndicator("远程");
		remoteSpec.setContent(remoteIntent);
		tabHost.addTab(remoteSpec);

		// 生成一个Intent对象，该对象指向一个Activity
		Intent localIntent = new Intent();
		localIntent.setClass(this, LocalListActivity.class);

		TabHost.TabSpec locaSpec = tabHost.newTabSpec("本地");
		locaSpec.setIndicator("本地");
		locaSpec.setContent(localIntent);
		tabHost.addTab(locaSpec);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
