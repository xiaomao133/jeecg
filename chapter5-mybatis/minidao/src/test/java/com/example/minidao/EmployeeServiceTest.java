package com.example.minidao;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.minidao.entity.Employee;
import com.example.minidao.service.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceTest {

	@Autowired
	private EmployeeService employeeService;

	@Test
	public void save() {
		Employee employee = new Employee();
		employee.setName("test");
		employee.setAge(20);
		employee.setBirthday(new Date());

		employeeService.save(employee);
		System.out.println("save:" + employee.getId());
	}

	@Test
	public void update() {
		Employee employee = employeeService.get("402881276030ae5e016030ae999f0000");
		employee.setName("2333");
		employee.setAge(22);
		employeeService.update(employee);
		System.out.println("update:" + employee.getId());
	}

	@Test
	public void delete() {
		employeeService.delete("402881276030ae5e016030ae999f0000");
		System.out.println("delete:ok");
	}

	@Test
	public void get() {
		Employee employee = employeeService.get("402881276030ae5e016030ae999f0000");
		System.out.println("get:" + employee);
	}

	@Test
	public void getCount() {
		Integer count = employeeService.getCount();
		System.out.println("getCount:" + count);
	}

	@Test
	public void getAll() {
		List<Employee> employees = employeeService.getAll(new Employee());
		System.out.println("getAll:" + employees.size());
	}

}
