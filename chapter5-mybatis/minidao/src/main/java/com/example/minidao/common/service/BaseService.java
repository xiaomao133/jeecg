package com.example.minidao.common.service;

import java.io.Serializable;

public interface BaseService<T, ID extends Serializable> {

	public T save(T entity);

	public T update(T entity);

	public void delete(T entity);

	public void delete(ID id);

	public T get(ID id);
}
