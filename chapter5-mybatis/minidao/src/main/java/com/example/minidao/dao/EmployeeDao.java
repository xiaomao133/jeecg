package com.example.minidao.dao;

import java.util.List;

import org.jeecgframework.minidao.annotation.Arguments;
import org.jeecgframework.minidao.annotation.Sql;
import org.springframework.stereotype.Repository;

import com.example.minidao.entity.Employee;

@Repository
public interface EmployeeDao {

	@Sql("select count(*) from t_employee")
	Integer getCount();

	@Arguments({ "employee" })
	List<Employee> getAll(Employee employee);
}
