package com.example.minidao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.minidao.common.service.BaseServiceImpl;
import com.example.minidao.dao.EmployeeDao;
import com.example.minidao.entity.Employee;
import com.example.minidao.service.EmployeeService;

@Service
public class EmployeeServiceImpl extends BaseServiceImpl<Employee, String> implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	@Override
	public Integer getCount() {
		return employeeDao.getCount();
	}

	@Override
	public List<Employee> getAll(Employee employee) {
		return employeeDao.getAll(employee);
	}
}
