package com.example.minidao.common.service;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.minidao.common.repository.BaseRepository;

public class BaseServiceImpl<T, ID extends Serializable> implements BaseService<T, ID> {

	@Autowired
	protected BaseRepository<T, ID> baseRepository;

	@Override
	public T save(T entity) {
		return baseRepository.save(entity);
	}

	@Override
	public T update(T entity) {
		return save(entity);
	}

	@Override
	public void delete(T entity) {
		baseRepository.delete(entity);
	}

	@Override
	public void delete(ID id) {
		baseRepository.delete(id);

	}

	@Override
	public T get(ID id) {
		return baseRepository.findOne(id);
	}

}
