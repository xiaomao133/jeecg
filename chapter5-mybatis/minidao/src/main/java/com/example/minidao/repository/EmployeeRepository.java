package com.example.minidao.repository;

import com.example.minidao.common.repository.BaseRepository;
import com.example.minidao.entity.Employee;

public interface EmployeeRepository extends BaseRepository<Employee, String> {

}
