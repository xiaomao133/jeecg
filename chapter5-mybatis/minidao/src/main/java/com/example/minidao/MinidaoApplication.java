package com.example.minidao;

import org.jeecgframework.minidao.factory.MiniDaoBeanScannerConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.example.minidao.common.repository.BaseRepositoryImpl;
import com.example.minidao.interceptor.MinidaoInterceptor;

@SpringBootApplication
@EnableJpaRepositories(repositoryBaseClass = BaseRepositoryImpl.class)
public class MinidaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinidaoApplication.class, args);
	}

	@Bean
	public MinidaoInterceptor minidaoInterceptor() {
		return new MinidaoInterceptor();
	}

	@Bean
	public MiniDaoBeanScannerConfigurer miniDaoBeanScannerConfigurer() {
		MiniDaoBeanScannerConfigurer miniDaoBeanScannerConfigurer = new MiniDaoBeanScannerConfigurer();
		miniDaoBeanScannerConfigurer.setKeyType("lower");
		miniDaoBeanScannerConfigurer.setFormatSql(true);
		miniDaoBeanScannerConfigurer.setShowSql(true);
		miniDaoBeanScannerConfigurer.setDbType("mysql");
		miniDaoBeanScannerConfigurer.setBasePackage("com.example.minidao.dao");
		miniDaoBeanScannerConfigurer.setAnnotation(org.springframework.stereotype.Repository.class);
		miniDaoBeanScannerConfigurer.setEmptyInterceptor(minidaoInterceptor());
		return miniDaoBeanScannerConfigurer;
	}
}
