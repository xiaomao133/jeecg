package com.example.minidao.service;

import java.util.List;

import com.example.minidao.common.service.BaseService;
import com.example.minidao.entity.Employee;

public interface EmployeeService extends BaseService<Employee, String> {

	Integer getCount();

	List<Employee> getAll(Employee employee);
}
