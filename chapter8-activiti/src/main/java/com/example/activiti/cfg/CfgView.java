package com.example.activiti.cfg;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;

@Configuration
public class CfgView extends org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter {
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("/index.html");
	}
}
