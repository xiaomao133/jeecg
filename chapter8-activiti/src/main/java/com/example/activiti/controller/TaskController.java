package com.example.activiti.controller;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.activiti.vo.TaskResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping({ "task" })
public class TaskController {
	@Autowired
	TaskService taskService;

	@GetMapping
	public List<TaskResponse> getList() throws JsonProcessingException {
		List<TaskResponse> list = new ArrayList();
		List<Task> tasks = this.taskService.createTaskQuery().list();
		for (Task task : tasks) {
			list.add(new TaskResponse(task));
		}
		return list;
	}

	@PostMapping({ "/complete/{id}" })
	public boolean complete(@PathVariable("id") String id) {
		this.taskService.complete(id);
		return true;
	}
}
