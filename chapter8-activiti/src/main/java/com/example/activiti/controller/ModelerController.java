package com.example.activiti.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.activiti.vo.ModelResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping({ "models" })
public class ModelerController {
	@Autowired
	RepositoryService repositoryService;
	@Autowired
	ObjectMapper objectMapper;

	@PostMapping({ "newModel" })
	public String newModel() throws UnsupportedEncodingException {
		Model model = this.repositoryService.newModel();

		String name = "new-process";
		String description = "";
		int revision = 1;
		String key = "process";

		ObjectNode modelNode = this.objectMapper.createObjectNode();
		modelNode.put("name", name);
		modelNode.put("description", description);
		modelNode.put("revision", revision);

		model.setName(name);
		model.setKey(key);
		model.setMetaInfo(modelNode.toString());

		this.repositoryService.saveModel(model);
		String id = model.getId();

		ObjectNode editorNode = this.objectMapper.createObjectNode();
		editorNode.put("id", "canvas");
		editorNode.put("resourceId", "canvas");
		ObjectNode stencilSetNode = this.objectMapper.createObjectNode();
		stencilSetNode.put("namespace", "http://b3mn.org/stencilset/bpmn2.0#");
		editorNode.put("stencilset", stencilSetNode);
		this.repositoryService.addModelEditorSource(id, editorNode.toString().getBytes("utf-8"));

		return "/modeler.html?modelId=" + id;
	}

	@GetMapping
	public List<ModelResponse> getList() {
		List<ModelResponse> list = new ArrayList();
		List<Model> models = this.repositoryService.createModelQuery().list();
		for (Model model : models) {
			list.add(new ModelResponse(model));
		}
		return list;
	}

	@PostMapping({ "del/{id}" })
	public Boolean delete(@PathVariable("id") String id) {
		this.repositoryService.deleteModel(id);
		return Boolean.valueOf(true);
	}

	@PostMapping({ "{id}/deployment" })
	public Boolean deploy(@PathVariable("id") String id) throws Exception {
		Model modelData = this.repositoryService.getModel(id);
		byte[] bytes = this.repositoryService.getModelEditorSource(modelData.getId());

		JsonNode modelNode = new ObjectMapper().readTree(bytes);

		BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);

		byte[] bpmnBytes = new BpmnXMLConverter().convertToXML(model);

		String processName = modelData.getName() + ".bpmn20.xml";
		Deployment deployment = this.repositoryService.createDeployment().name(modelData.getName())
				.addString(processName, new String(bpmnBytes, "UTF-8")).deploy();
		modelData.setDeploymentId(deployment.getId());
		this.repositoryService.saveModel(modelData);

		return Boolean.valueOf(true);
	}
}
