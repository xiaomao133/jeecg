package com.example.activiti.controller;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.activiti.vo.DeploymentResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping({ "deployments" })
public class DeploymentController {
	@Autowired
	RepositoryService repositoryService;

	@GetMapping
	public List<DeploymentResponse> getList() throws JsonProcessingException {
		List<DeploymentResponse> list = new ArrayList();
		List<Deployment> deployments = this.repositoryService.createDeploymentQuery().list();
		for (Deployment deployment : deployments) {
			list.add(new DeploymentResponse(deployment));
		}
		return list;
	}

	@PostMapping("del/{id}")
	public Boolean deleteOne(@PathVariable("id") String id) {
		this.repositoryService.deleteDeployment(id);
		return Boolean.valueOf(true);
	}
}
