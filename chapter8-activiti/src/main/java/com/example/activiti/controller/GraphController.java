package com.example.activiti.controller;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.cmd.GetDeploymentProcessDiagramCmd;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.activiti.cmd.ProcessInstanceDiagramCmd;

@Controller
@RequestMapping({ "graph" })
public class GraphController {
	@Autowired
	ProcessEngine processEngine;
	@Autowired
	TaskService taskService;

	@GetMapping("/{type}/{id}")
	public void graph(@PathVariable("type") String type, @PathVariable("id") String id, HttpServletResponse response) {
		Command<InputStream> command = null;
		if (type.equals("pd")) {
			command = new GetDeploymentProcessDiagramCmd(id);
		} else if (type.equals("pi")) {
			command = new ProcessInstanceDiagramCmd(id);
		} else if (type.equals("task")) {
			Task task = taskService.createTaskQuery().taskId(id).singleResult();
			command = new ProcessInstanceDiagramCmd(task.getProcessInstanceId());
		}

		if (command != null) {
			InputStream is = processEngine.getManagementService().executeCommand(command);

			try {
				int len = 0;
				byte[] buffer = new byte[1024 * 10];
				while ((len = is.read(buffer)) != -1) {
					response.getOutputStream().write(buffer, 0, len);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
