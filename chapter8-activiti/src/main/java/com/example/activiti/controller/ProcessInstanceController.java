package com.example.activiti.controller;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.activiti.vo.ProcessInstanceResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping({ "processInstance" })
public class ProcessInstanceController {
	@Autowired
	RuntimeService runtimeService;

	@GetMapping
	public List<ProcessInstanceResponse> getList() throws JsonProcessingException {
		List<ProcessInstanceResponse> list = new ArrayList();
		List<ProcessInstance> processInstances = this.runtimeService.createProcessInstanceQuery().list();
		for (ProcessInstance processInstance : processInstances) {
			list.add(new ProcessInstanceResponse(processInstance));
		}
		return list;
	}
}
