package com.example.activiti.controller;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.activiti.vo.ProcessDefinitionResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping({ "processDefinitions" })
public class ProcessDefinitionController {
	@Autowired
	RepositoryService repositoryService;
	@Autowired
	RuntimeService runtimeService;

	@GetMapping
	public List<ProcessDefinitionResponse> getList() throws JsonProcessingException {
		List<ProcessDefinitionResponse> list = new ArrayList();
		List<ProcessDefinition> processDefinitions = this.repositoryService.createProcessDefinitionQuery().list();
		for (ProcessDefinition processDefinition : processDefinitions) {
			list.add(new ProcessDefinitionResponse(processDefinition));
		}

		return list;
	}

	@PostMapping({ "start/{id}" })
	public boolean start(@PathVariable("id") String id) {
		this.runtimeService.startProcessInstanceById(id);
		return true;
	}
}
