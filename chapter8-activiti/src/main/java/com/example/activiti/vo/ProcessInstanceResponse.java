package com.example.activiti.vo;

import java.util.Map;

import org.activiti.engine.runtime.ProcessInstance;

public class ProcessInstanceResponse {
	private String id;
	private boolean ended;
	private String activityId;
	private String processInstanceId;
	private String parentId;
	private String superExecutionId;
	private String processDefinitionId;
	private String processDefinitionName;
	private String processDefinitionKey;
	private Integer processDefinitionVersion;
	private String deploymentId;
	private String businessKey;
	private boolean suspended;
	private Map<String, Object> processVariables;
	private String tenantId;
	private String name;
	private String description;
	private String localizedName;
	private String localizedDescription;

	public ProcessInstanceResponse(ProcessInstance processInstance) {
		setId(processInstance.getId());
		setEnded(processInstance.isEnded());
		setActivityId(processInstance.getActivityId());
		setProcessInstanceId(processInstance.getProcessInstanceId());
		setParentId(processInstance.getParentId());
		setSuperExecutionId(processInstance.getSuperExecutionId());

		setProcessDefinitionId(processInstance.getProcessDefinitionId());
		setProcessDefinitionName(processInstance.getProcessDefinitionName());
		setProcessDefinitionKey(processInstance.getProcessDefinitionKey());
		setProcessDefinitionVersion(processInstance.getProcessDefinitionVersion());
		setDeploymentId(processInstance.getDeploymentId());
		setBusinessKey(processInstance.getBusinessKey());
		setSuspended(processInstance.isSuspended());
		setProcessVariables(processInstance.getProcessVariables());
		setTenantId(processInstance.getTenantId());
		setName(processInstance.getName());
		setDescription(processInstance.getDescription());
		setLocalizedName(processInstance.getLocalizedName());
		setLocalizedDescription(processInstance.getLocalizedDescription());
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isEnded() {
		return this.ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

	public String getActivityId() {
		return this.activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getProcessInstanceId() {
		return this.processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getParentId() {
		return this.parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getSuperExecutionId() {
		return this.superExecutionId;
	}

	public void setSuperExecutionId(String superExecutionId) {
		this.superExecutionId = superExecutionId;
	}

	public String getProcessDefinitionId() {
		return this.processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getProcessDefinitionName() {
		return this.processDefinitionName;
	}

	public void setProcessDefinitionName(String processDefinitionName) {
		this.processDefinitionName = processDefinitionName;
	}

	public String getProcessDefinitionKey() {
		return this.processDefinitionKey;
	}

	public void setProcessDefinitionKey(String processDefinitionKey) {
		this.processDefinitionKey = processDefinitionKey;
	}

	public Integer getProcessDefinitionVersion() {
		return this.processDefinitionVersion;
	}

	public void setProcessDefinitionVersion(Integer processDefinitionVersion) {
		this.processDefinitionVersion = processDefinitionVersion;
	}

	public String getDeploymentId() {
		return this.deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getBusinessKey() {
		return this.businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	public boolean isSuspended() {
		return this.suspended;
	}

	public void setSuspended(boolean suspended) {
		this.suspended = suspended;
	}

	public Map<String, Object> getProcessVariables() {
		return this.processVariables;
	}

	public void setProcessVariables(Map<String, Object> processVariables) {
		this.processVariables = processVariables;
	}

	public String getTenantId() {
		return this.tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocalizedName() {
		return this.localizedName;
	}

	public void setLocalizedName(String localizedName) {
		this.localizedName = localizedName;
	}

	public String getLocalizedDescription() {
		return this.localizedDescription;
	}

	public void setLocalizedDescription(String localizedDescription) {
		this.localizedDescription = localizedDescription;
	}
}
