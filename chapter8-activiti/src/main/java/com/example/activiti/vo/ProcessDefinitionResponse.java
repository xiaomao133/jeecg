package com.example.activiti.vo;

import org.activiti.engine.repository.ProcessDefinition;

public class ProcessDefinitionResponse {
	private String id;
	private String category;
	private String name;
	private String key;
	private String description;
	private int version;
	private String resourceName;
	private String deploymentId;
	private String diagramResourceName;
	private boolean hasStartFormKey;
	private boolean hasGraphicalNotation;
	private boolean isSuspended;
	private String tenantId;

	public ProcessDefinitionResponse(ProcessDefinition processDefinition) {
		setId(processDefinition.getId());
		setCategory(processDefinition.getCategory());
		setName(processDefinition.getName());
		setKey(processDefinition.getKey());
		setDescription(processDefinition.getDescription());
		setVersion(processDefinition.getVersion());
		setResourceName(processDefinition.getResourceName());
		setDeploymentId(processDefinition.getDeploymentId());
		setDiagramResourceName(processDefinition.getDiagramResourceName());
		setHasStartFormKey(processDefinition.hasStartFormKey());
		setHasGraphicalNotation(processDefinition.hasGraphicalNotation());
		setSuspended(processDefinition.isSuspended());
		setTenantId(processDefinition.getTenantId());
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getResourceName() {
		return this.resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getDeploymentId() {
		return this.deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getDiagramResourceName() {
		return this.diagramResourceName;
	}

	public void setDiagramResourceName(String diagramResourceName) {
		this.diagramResourceName = diagramResourceName;
	}

	public boolean isHasStartFormKey() {
		return this.hasStartFormKey;
	}

	public void setHasStartFormKey(boolean hasStartFormKey) {
		this.hasStartFormKey = hasStartFormKey;
	}

	public boolean isHasGraphicalNotation() {
		return this.hasGraphicalNotation;
	}

	public void setHasGraphicalNotation(boolean hasGraphicalNotation) {
		this.hasGraphicalNotation = hasGraphicalNotation;
	}

	public boolean isSuspended() {
		return this.isSuspended;
	}

	public void setSuspended(boolean isSuspended) {
		this.isSuspended = isSuspended;
	}

	public String getTenantId() {
		return this.tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}
