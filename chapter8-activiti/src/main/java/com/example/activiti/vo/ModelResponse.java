package com.example.activiti.vo;

import java.util.Date;

import org.activiti.engine.repository.Model;
import org.activiti.rest.common.util.DateToStringSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ModelResponse {
	private String id;
	private String name;
	private String key;
	private String category;
	@JsonSerialize(using = DateToStringSerializer.class, as = Date.class)
	private Date createTime;
	@JsonSerialize(using = DateToStringSerializer.class, as = Date.class)
	private Date lastUpdateTime;
	private Integer version;
	private String metaInfo;
	private String deploymentId;
	private String tenantId;
	private boolean hasEditorSource;
	private boolean hasEditorSourceExtra;

	public ModelResponse(Model model) {
		setId(model.getId());
		setName(model.getName());
		setKey(model.getKey());
		setCategory(model.getCategory());
		setCreateTime(model.getCreateTime());
		setLastUpdateTime(model.getLastUpdateTime());
		setVersion(model.getVersion());
		setMetaInfo(model.getMetaInfo());
		setDeploymentId(model.getDeploymentId());
		setTenantId(model.getTenantId());
		setHasEditorSource(model.hasEditorSource());
		setHasEditorSourceExtra(model.hasEditorSourceExtra());
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getLastUpdateTime() {
		return this.lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getMetaInfo() {
		return this.metaInfo;
	}

	public void setMetaInfo(String metaInfo) {
		this.metaInfo = metaInfo;
	}

	public String getDeploymentId() {
		return this.deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getTenantId() {
		return this.tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public boolean isHasEditorSource() {
		return this.hasEditorSource;
	}

	public void setHasEditorSource(boolean hasEditorSource) {
		this.hasEditorSource = hasEditorSource;
	}

	public boolean isHasEditorSourceExtra() {
		return this.hasEditorSourceExtra;
	}

	public void setHasEditorSourceExtra(boolean hasEditorSourceExtra) {
		this.hasEditorSourceExtra = hasEditorSourceExtra;
	}
}
