package com.example.activiti.vo;

import java.util.Map;

import org.activiti.engine.task.Task;

public class TaskResponse {
	private String id;
	private String name;
	private String description;
	private int priority;
	private String owner;
	private String assignee;
	private String processInstanceId;
	private String executionId;
	private String processDefinitionId;
	private java.util.Date createTime;
	private String taskDefinitionKey;
	private java.util.Date dueDate;
	private String category;
	private String parentTaskId;
	private String tenantId;
	private String formKey;
	private Map<String, Object> taskLocalVariables;
	private Map<String, Object> processVariables;

	public TaskResponse(Task task) {
		setId(task.getId());
		setName(task.getName());
		setDescription(task.getDescription());
		setPriority(task.getPriority());
		setOwner(task.getOwner());
		setAssignee(task.getAssignee());
		setProcessInstanceId(task.getProcessInstanceId());
		setExecutionId(task.getExecutionId());
		setProcessDefinitionId(task.getProcessDefinitionId());
		setCreateTime(task.getCreateTime());
		setTaskDefinitionKey(task.getTaskDefinitionKey());
		setDueDate(task.getDueDate());
		setCategory(task.getCategory());
		setParentTaskId(task.getParentTaskId());
		setTenantId(task.getTenantId());
		setFormKey(task.getFormKey());
		setTaskLocalVariables(task.getTaskLocalVariables());
		setProcessVariables(task.getProcessVariables());
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getAssignee() {
		return this.assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getProcessInstanceId() {
		return this.processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getExecutionId() {
		return this.executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public String getProcessDefinitionId() {
		return this.processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	public String getTaskDefinitionKey() {
		return this.taskDefinitionKey;
	}

	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}

	public java.util.Date getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(java.util.Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getParentTaskId() {
		return this.parentTaskId;
	}

	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public String getTenantId() {
		return this.tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getFormKey() {
		return this.formKey;
	}

	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}

	public Map<String, Object> getTaskLocalVariables() {
		return this.taskLocalVariables;
	}

	public void setTaskLocalVariables(Map<String, Object> taskLocalVariables) {
		this.taskLocalVariables = taskLocalVariables;
	}

	public Map<String, Object> getProcessVariables() {
		return this.processVariables;
	}

	public void setProcessVariables(Map<String, Object> processVariables) {
		this.processVariables = processVariables;
	}
}
