package com.example.activiti.cmd;

import java.io.InputStream;
import java.util.Collections;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.impl.cmd.GetBpmnModelCmd;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.ExecutionEntityManager;
import org.activiti.image.ProcessDiagramGenerator;

public class ProcessInstanceDiagramCmd implements Command<InputStream> {
	protected String processInstanceId;

	public ProcessInstanceDiagramCmd(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public InputStream execute(CommandContext commandContext) {
		ExecutionEntityManager executionEntityManager = commandContext.getExecutionEntityManager();
		ExecutionEntity executionEntity = executionEntityManager.findExecutionById(this.processInstanceId);

		java.util.List<String> activityIds = executionEntity.findActiveActivityIds();
		String processDefinitionId = executionEntity.getProcessDefinitionId();

		GetBpmnModelCmd getBpmnModelCmd = new GetBpmnModelCmd(processDefinitionId);
		BpmnModel bpmnModel = getBpmnModelCmd.execute(commandContext);

		ProcessDiagramGenerator processDiagramGenerator = commandContext.getProcessEngineConfiguration()
				.getProcessDiagramGenerator();

		InputStream is = processDiagramGenerator.generateDiagram(bpmnModel, "png", activityIds,
				Collections.<String> emptyList(), commandContext.getProcessEngineConfiguration().getActivityFontName(),
				commandContext.getProcessEngineConfiguration().getLabelFontName(),
				commandContext.getProcessEngineConfiguration().getAnnotationFontName(), null, 1.0D);
		return is;
	}
}
