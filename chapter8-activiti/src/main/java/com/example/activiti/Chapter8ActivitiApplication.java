package com.example.activiti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.example.activiti", "org.activiti.rest.editor" })
public class Chapter8ActivitiApplication {
	public static void main(String[] args) {
		SpringApplication.run(Chapter8ActivitiApplication.class, args);
	}
}
