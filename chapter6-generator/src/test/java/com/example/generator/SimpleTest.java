package com.example.generator;

import cn.org.rapid_framework.generator.GeneratorFacade;

public class SimpleTest {
	public static void main(String[] args) throws Exception {
		GeneratorFacade g = new GeneratorFacade();
		g.deleteOutRootDir();
		g.getGenerator().setTemplateRootDir(System.getProperty("user.dir") + "/src/main/resources/template");
		g.generateByTable("t_employee");
	}
}
