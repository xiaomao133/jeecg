package com.example.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Chapter6GeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(Chapter6GeneratorApplication.class, args);
	}
}
