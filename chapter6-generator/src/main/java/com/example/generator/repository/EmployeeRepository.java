package com.example.generator.repository;

import com.example.generator.common.repository.BaseRepository;
import com.example.generator.entity.Employee;

public interface EmployeeRepository extends BaseRepository<Employee, String> {

}
