package com.example.generator.entity;

import java.util.Date;

import lombok.Data;

@Data
public class Employee {

	/**
	 * 
	 */
	private String id;
	/**
	 * 
	 */
	private Integer age;
	/**
	 * 
	 */
	private Date birthday;
	/**
	 * 
	 */
	private String createBy;
	/**
	 * 
	 */
	private Date createDate;
	/**
	 * 
	 */
	private String empno;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private Long salary;
	/**
	 * 
	 */
	private String updateBy;
	/**
	 * 
	 */
	private Date updateDate;

}