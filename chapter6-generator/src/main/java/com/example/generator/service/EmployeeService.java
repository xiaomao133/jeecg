package com.example.generator.service;

import com.example.generator.common.service.BaseService;
import com.example.generator.entity.Employee;

public interface EmployeeService extends BaseService<Employee, String> {

}
