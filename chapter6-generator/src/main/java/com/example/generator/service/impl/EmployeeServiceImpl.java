package com.example.generator.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.generator.common.service.BaseServiceImpl;
import com.example.generator.entity.Employee;
import com.example.generator.repository.EmployeeRepository;
import com.example.generator.service.EmployeeService;

@Service
public class EmployeeServiceImpl extends BaseServiceImpl<Employee, String> implements EmployeeService {
	@Autowired
	private EmployeeRepository employeeRepository;
}
