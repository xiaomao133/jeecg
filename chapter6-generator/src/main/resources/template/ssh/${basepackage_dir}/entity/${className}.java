<#assign className = table.className>
package ${basepackage}.entity;

<#list table.columns as column>
    <#if column.simpleJavaType == "Date">
import java.util.Date;
    <#break>
    </#if>
</#list>

import lombok.Data;

@Data
public class ${className} {

<#list table.columns as column>
	/**
	 * ${column.remarks}
	 */
	private ${column.simpleJavaType} ${column.columnNameFirstLower};
</#list>

}