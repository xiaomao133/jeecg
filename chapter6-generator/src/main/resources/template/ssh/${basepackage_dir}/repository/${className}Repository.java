<#assign className = table.className>
package ${basepackage}.repository;

import ${basepackage}.common.repository.BaseRepository;
import ${basepackage}.entity.${className};

public interface ${className}Repository extends BaseRepository<${className}, String> {

}
