<#assign className = table.className>
package ${basepackage}.service;

import ${basepackage}.common.service.BaseService;
import ${basepackage}.entity.${className};

public interface ${className}Service extends BaseService<${className}, String> {

}
