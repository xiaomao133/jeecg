<#assign className = table.className>
<#assign classNameLower = className?uncap_first> 
package ${basepackage}.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ${basepackage}.common.service.BaseServiceImpl;
import ${basepackage}.entity.${className};
import ${basepackage}.repository.${className}Repository;
import ${basepackage}.service.${className}Service;

@Service
public class ${className}ServiceImpl extends BaseServiceImpl<${className}, String> implements ${className}Service {
	@Autowired
	private ${className}Repository ${classNameLower}Repository;
}
