package com.maolei.zk.lock.zkLock;

import java.util.concurrent.CountDownLatch;

import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.exception.ZkException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZookeeperLock extends ZookeeperAbstractLock {
	private static Logger logger = LoggerFactory.getLogger(ZookeeperLock.class);
	
	private CountDownLatch cdl = null;

	@Override
	protected void waitForLock() {
		logger.info(Thread.currentThread().getName() + ", waitForLock");
		// 创建数据监听器
		IZkDataListener listener = new IZkDataListener() {
			// 数据被删除，触发的方法
			public void handleDataDeleted(String dataPath) throws Exception {
				if (cdl != null) {
					cdl.countDown();
				}
			}

			// 数据被修改，触发的方法
			public void handleDataChange(String dataPath, Object data) throws Exception {

			}
		};

		logger.info(Thread.currentThread().getName() + ", lockPath：" + lockPath);
		zkClient.subscribeDataChanges(lockPath, listener);
		/**
		 * 判断节点是否存在，存在 就等待锁释放
		 */
		if (zkClient.exists(lockPath)) {
			cdl = new CountDownLatch(1);
			try {
				cdl.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		// 删除数据监听器
		zkClient.unsubscribeDataChanges(lockPath, listener);
	}

	@Override
	protected boolean tryLock() {
		logger.info(Thread.currentThread().getName() + ", tryLock");
		try {
			zkClient.createEphemeral(lockPath);
			return true;
		} catch (ZkException e) {
			return false;
		}
	}

}
