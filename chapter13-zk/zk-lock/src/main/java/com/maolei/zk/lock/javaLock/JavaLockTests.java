package com.maolei.zk.lock.javaLock;

import java.util.concurrent.CountDownLatch;

public class JavaLockTests implements Runnable {
	private OrderService orderService = new OrderService();

	private static int count = 500;
	// 控制同时启动线程
	private static CountDownLatch cdl = new CountDownLatch(count);
	// 互斥量
	private static Object mutex = new Object();

	public void run() {
		try {
			// 阻塞住当前线程,等待计数器减少到0
			cdl.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// 互斥锁
		synchronized (mutex) {
			orderService.createOrderLocal();
		}
	}

	public static void main(String[] args) {
		for (int i = 0; i < 500; i++) {
			new Thread(new JavaLockTests()).start();
			// 向下减少计数器
			cdl.countDown();
		}
	}

}
