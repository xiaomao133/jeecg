package com.maolei.zk.lock.zkLock;

import java.util.concurrent.CountDownLatch;

import com.maolei.zk.lock.javaLock.OrderService;

public class ZkLockTests implements Runnable {
	private OrderService orderService = new OrderService();

	private static int count = 100;
	// 控制同时启动线程
	private static CountDownLatch cdl = new CountDownLatch(count);

	public void run() {
		try {
			// 阻塞住当前线程,等待计数器减少到0
			cdl.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		orderService.createOrderRemote();
	}

	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();

		for (int i = 0; i < count; i++) {
			new Thread(new ZkLockTests()).start();
			// 向下减少计数器
			cdl.countDown();
		}

		long endTime = System.currentTimeMillis();
		float excTime = (float) (endTime - startTime) / 1000;
		System.out.println("执行时间：" + excTime + "s");
	}

}
