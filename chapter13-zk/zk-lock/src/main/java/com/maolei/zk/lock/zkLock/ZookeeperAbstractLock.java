package com.maolei.zk.lock.zkLock;

import org.I0Itec.zkclient.ZkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ZookeeperAbstractLock implements Lock {
	private static Logger logger = LoggerFactory.getLogger(ZookeeperAbstractLock.class);

	// private static String zkAdress = "172.96.231.195:2181";
	private static String zkAdress = "192.168.168.132:2181";

	protected static String lockPath = "/lock";

	protected ZkClient zkClient = new ZkClient(zkAdress);

	// 使用临时数据，阻塞时的获取锁
	public void getLock() {
		if (tryLock()) {
			logger.info(Thread.currentThread().getName() + ", get lock!");
		} else {
			logger.info(Thread.currentThread().getName() + ", not lock!");
			waitForLock();
			getLock();
		}
	}

	protected abstract void waitForLock();

	protected abstract boolean tryLock();

	// 使用的临时数据，只要关闭客户端数据自动清理
	public void unLock() {
		zkClient.close();
	}

}
