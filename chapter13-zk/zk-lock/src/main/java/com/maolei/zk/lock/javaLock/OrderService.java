package com.maolei.zk.lock.javaLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.maolei.zk.lock.zkLock.Lock;
import com.maolei.zk.lock.zkLock.ZookeeperLock;

public class OrderService {
	private static Logger logger = LoggerFactory.getLogger(OrderService.class);

	private OrderNumGenerator ong = new OrderNumGenerator();
	private Lock lock = new ZookeeperLock();

	// 本地java锁
	public void createOrderLocal() {
		String orderNum = ong.getOrderNum();
		logger.info(Thread.currentThread().getName() + ",获得订单号：" + orderNum);
	}

	// 远程zk锁
	public void createOrderRemote() {
		logger.info(Thread.currentThread().getName() + ",createOrderRemote");
		lock.getLock();
		createOrderLocal();
		lock.unLock();
	}

}
