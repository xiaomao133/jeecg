package com.maolei.zk.lock.zkLock;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.I0Itec.zkclient.IZkDataListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 增强锁，避免羊群效应
 */
public class ZookeeperImproveLock extends ZookeeperAbstractLock {
	private static Logger logger = LoggerFactory.getLogger(ZookeeperImproveLock.class);

	private String beforePath;
	private String currentPath;
	private CountDownLatch latch = null;

	public ZookeeperImproveLock() {
		if (!this.zkClient.exists(lockPath)) {
			this.zkClient.createPersistent(lockPath, "lock");
		}
	}

	@Override
	protected void waitForLock() {
		logger.info(Thread.currentThread().getName() + ", waitForLock");
		IZkDataListener listener = new IZkDataListener() {

			public void handleDataDeleted(String dataPath) throws Exception {
				if (latch != null) {
					latch.countDown();
				}
			}

			public void handleDataChange(String dataPath, Object data) throws Exception {
			}
		};

		logger.info(Thread.currentThread().getName() + ", beforePath：" + beforePath);
		this.zkClient.subscribeDataChanges(beforePath, listener);

		if (this.zkClient.exists(beforePath)) {
			latch = new CountDownLatch(1);
			try {
				latch.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.zkClient.unsubscribeDataChanges(beforePath, listener);
	}

	@Override
	protected boolean tryLock() {
		logger.info(Thread.currentThread().getName() + ", tryLock");
		logger.info(Thread.currentThread().getName() + ",之前：" + currentPath);
		if (currentPath == null || currentPath.length() <= 0) {
			currentPath = this.zkClient.createEphemeralSequential(lockPath + "/", "lock");
		}
		logger.info(Thread.currentThread().getName() + ",之后：" + currentPath);

		List<String> childrens = this.zkClient.getChildren(lockPath);
		Collections.sort(childrens);
		if (currentPath.equals(lockPath + "/" + childrens.get(0))) {
			return true;
		} else {
			int wz = Collections.binarySearch(childrens, currentPath.substring(6));
			beforePath = lockPath + "/" + childrens.get(wz - 1);
		}
		return false;
	}

}
