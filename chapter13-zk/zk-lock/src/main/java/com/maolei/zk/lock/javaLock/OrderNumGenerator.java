package com.maolei.zk.lock.javaLock;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderNumGenerator {
	private static int i = 0;

	public String getOrderNum() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss-");
		return sdf.format(new Date()) + ++i;

	}
}
