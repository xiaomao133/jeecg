package com.maolei.zk.config.tests;

import org.junit.Test;

import com.maolei.zk.config.ClientApp;
import com.maolei.zk.config.ZKUtil;
import com.maolei.zk.config.bean.ZKConfig;
import com.maolei.zk.config.manager.ZKConfigManager;
import com.maolei.zk.config.manager.ZKMysqlManager;

public class ConfigTest {
	private ZKConfigManager configManager = new ZKMysqlManager();

	@Test
	public void testZkConfig() throws InterruptedException {

		ClientApp clientApp = new ClientApp();

		// 模拟【配置管理中心】初始化时，从db加载配置初始参数
		// configManager.loadConfigFromDB();
		// 然后将配置同步到ZK
		// configManager.syncConfigToZk(ZKUtil.MYSQL_CONFIG_NODE_NAME);

		configManager.updateConfigToDB(new ZKConfig("jdbc:mysql://66.112.220.151:3306/sspanel", 0, "1", "2"));
		//configManager.updateConfigToDB(new ZKConfig("jdbc:mysql://192.168.168.132:3306/sspanel", 0, "1", "2"));
		configManager.syncConfigToZk(ZKUtil.MYSQL_CONFIG_NODE_NAME);

		// 模拟客户端程序运行
		// clientApp.run();

		// 模拟配置修改
		// ZKConfig config = new ZKConfig("10.6.12.34", 23, "newUser",
		// "newPwd");
		// configManager.updateConfigToDB();
		// configManager.syncConfigToZk(ZKUtil.MYSQL_CONFIG_NODE_NAME);

		// 模拟客户端自动感知配置变化
		// clientApp.run();

		// 模拟配置修改
		// config.setAll("110.110.110.110", 66, "aaaaa", "bbbbb");
		// configManager.updateConfigToDB();
		// configManager.syncConfigToZk(ZKUtil.MYSQL_CONFIG_NODE_NAME);

		// 模拟客户端自动感知配置变化
		// clientApp.run();

	}

}
