package com.maolei.zk.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ApplicationContext;

@ServletComponentScan
@SpringBootApplication
public class ZkConfigApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(ZkConfigApplication.class, args);
	}
}
