package com.maolei.zk.config.bean;

import java.io.Serializable;

public class ZKFtpConfig extends ZKConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	public ZKFtpConfig() {
	}

	public ZKFtpConfig(String host, int port, String user, String password) {
		super(host, port, user, password);
	}
}
