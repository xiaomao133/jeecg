package com.maolei.zk.config;

import org.I0Itec.zkclient.ZkClient;

public class ZKUtil {

	private static final String SERVER_STRING = "172.96.231.195:2181,172.96.231.195:2182,172.96.231.195:2183";
	public static final String MYSQL_CONFIG_NODE_NAME = "/config/mysql";

	public static ZkClient getZkClient() {
		return new ZkClient(SERVER_STRING);
	}

}
