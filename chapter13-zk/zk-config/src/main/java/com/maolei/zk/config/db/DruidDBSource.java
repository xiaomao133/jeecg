package com.maolei.zk.config.db;

import java.sql.SQLException;

import com.alibaba.druid.pool.DruidDataSource;
import com.maolei.zk.config.bean.SpringUtil;
import com.maolei.zk.config.bean.ZKMysqlConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DruidDBSource extends DruidDataSource {
	private static final long serialVersionUID = 1L;

	public void initDataSource(ZKMysqlConfig mysqlConfig) {
		log.info(mysqlConfig.getHost());

		this.setUrl(mysqlConfig.getHost());
		this.setUsername(mysqlConfig.getUser());
		this.setPassword(mysqlConfig.getPassword());
		this.setDriverClassName(mysqlConfig.getDriverClassName());

		// configuration
		this.setInitialSize(mysqlConfig.getInitialSize());
		this.setMinIdle(mysqlConfig.getMinIdle());
		this.setMaxActive(mysqlConfig.getMaxActive());
		this.setMaxWait(mysqlConfig.getMaxWait());
		this.setTimeBetweenEvictionRunsMillis(mysqlConfig.getTimeBetweenEvictionRunsMillis());
		this.setMinEvictableIdleTimeMillis(mysqlConfig.getMinEvictableIdleTimeMillis());
		this.setValidationQuery(mysqlConfig.getValidationQuery());
		this.setTestWhileIdle(mysqlConfig.isTestWhileIdle());
		this.setTestOnBorrow(mysqlConfig.isTestOnBorrow());
		this.setTestOnReturn(mysqlConfig.isTestOnReturn());
		this.setPoolPreparedStatements(mysqlConfig.isPoolPreparedStatements());
		this.setMaxPoolPreparedStatementPerConnectionSize(mysqlConfig.getMaxPoolPreparedStatementPerConnectionSize());
	}

	public void changeDataSource(ZKMysqlConfig mysqlConfig) throws SQLException {
		log.info(mysqlConfig.getHost());

		DruidDBSource druidDBSource = (DruidDBSource) SpringUtil.getBean("dataSource");
		// 判断此时的连接池是否已经初始化,如果初始化了就重启,这里不能直接close，会报错
		if (druidDBSource.isInited()) {
			druidDBSource.restart();
		}
		druidDBSource.initDataSource(mysqlConfig);
		// 对连接池进行初始化,使设置生效
		druidDBSource.init();
	}
}
