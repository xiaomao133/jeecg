package com.maolei.zk.config.db;

import javax.sql.DataSource;

import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.maolei.zk.config.ZKUtil;
import com.maolei.zk.config.bean.ZKMysqlConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class DruidDBConfig {

	@Bean // 声明其为Bean实例
	@Primary // 在同样的DataSource中，首先使用被标注的DataSource
	public DataSource dataSource() {
		ZkClient zk = ZKUtil.getZkClient();
		ZKMysqlConfig mysqlConfig = zk.readData(ZKUtil.MYSQL_CONFIG_NODE_NAME);
		log.info("=========================dataSource=================================");
		log.info("{}", mysqlConfig);

		DruidDBSource druidDBSource = new DruidDBSource();
		zk.subscribeDataChanges(ZKUtil.MYSQL_CONFIG_NODE_NAME, new IZkDataListener() {

			@Override
			public void handleDataDeleted(String dataPath) throws Exception {

			}

			@Override
			public void handleDataChange(String dataPath, Object data) throws Exception {
				log.info("========================handleDataChange==================================");
				log.info("{}", data);
				druidDBSource.changeDataSource((ZKMysqlConfig) data);
			}
		});
		druidDBSource.initDataSource(mysqlConfig);
		return druidDBSource;
	}

}
