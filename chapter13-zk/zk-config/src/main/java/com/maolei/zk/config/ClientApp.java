package com.maolei.zk.config;

import java.util.concurrent.TimeUnit;

import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;

import com.maolei.zk.config.bean.ZKConfig;
import com.maolei.zk.config.bean.ZKMysqlConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ClientApp {
	ZKConfig mysqlConfig;

	private ZKConfig getZKMysqlConfig() {
		if (mysqlConfig == null) {
			ZkClient zk = ZKUtil.getZkClient();
			mysqlConfig = zk.readData(ZKUtil.MYSQL_CONFIG_NODE_NAME);
			log.info("getZKMysqlConfig ==> {}", mysqlConfig);

			zk.subscribeDataChanges(ZKUtil.MYSQL_CONFIG_NODE_NAME, new IZkDataListener() {

				@Override
				public void handleDataDeleted(String dataPath) throws Exception {
					log.info("mysqlConfig is deleted !");
					log.info("node ==>" + dataPath);
					mysqlConfig = null;
				}

				@Override
				public void handleDataChange(String dataPath, Object data) throws Exception {
					log.info("mysqlConfig is deleted !");
					log.info("node ==>" + dataPath);
					log.info("data ==>" + data);
					mysqlConfig = (ZKMysqlConfig) data;
				}
			});
		}
		return mysqlConfig;
	}

	public void run() throws InterruptedException {
		getZKMysqlConfig();

		upload();

		download();
	}

	public void upload() throws InterruptedException {
		log.info("正在上传文件...");
		log.info(mysqlConfig.toString());
		TimeUnit.SECONDS.sleep(10);
		log.info("文件上传完成...");
	}

	public void download() throws InterruptedException {
		log.info("正在下载文件...");
		log.info(mysqlConfig.toString());
		TimeUnit.SECONDS.sleep(10);
		log.info("文件下载完成...");
	}
}
