package com.maolei.zk.config.manager;

import com.maolei.zk.config.bean.ZKConfig;
import com.maolei.zk.config.bean.ZKMysqlConfig;

public class ZKMysqlManager extends ZKAbstractConfigManager {

	@Override
	protected ZKConfig loadFromDB() {
		ZKMysqlConfig config = new ZKMysqlConfig();
		config.setHost("jdbc:mysql://66.112.220.151:3306/sspanel");
		config.setUser("sspanel");
		config.setPassword("sspanel");
		config.setDriverClassName("com.mysql.jdbc.Driver");
		config.setInitialSize(5);
		config.setMinIdle(5);
		config.setMaxActive(20);
		config.setMaxWait(60000);
		config.setTimeBetweenEvictionRunsMillis(60000);
		config.setMinEvictableIdleTimeMillis(300000);
		config.setValidationQuery("SELECT 1 FROM DUAL");
		config.setTestWhileIdle(true);
		config.setTestOnBorrow(false);
		config.setTestOnReturn(false);
		config.setPoolPreparedStatements(true);
		config.setMaxPoolPreparedStatementPerConnectionSize(20);
		return config;
	}

}
