package com.maolei.zk.config.manager;

import com.maolei.zk.config.bean.ZKConfig;

public interface ZKConfigManager {

	/**
	 * 模拟从db加载初始配置
	 */
	public void loadConfigFromDB();

	/**
	 * 模拟更新DB中的配置
	 * 
	 * @param config
	 */
	public void updateConfigToDB(ZKConfig zkConfig);

	/**
	 * 将配置同步到ZK
	 * 
	 * @param path
	 */
	public void syncConfigToZk(String path);

}
