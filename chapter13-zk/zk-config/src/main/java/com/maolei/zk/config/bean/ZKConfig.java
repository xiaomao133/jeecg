package com.maolei.zk.config.bean;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ZKConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主机名或IP
	 */
	protected String host;

	/**
	 * 端口号
	 */
	protected int port;

	/**
	 * 连接用户名
	 */
	protected String user;

	/**
	 * 连接密码
	 */
	protected String password;

	public void setAll(String host, int port, String user, String password) {
		this.host = host;
		this.port = port;
		this.user = user;
		this.password = password;
	}
}
