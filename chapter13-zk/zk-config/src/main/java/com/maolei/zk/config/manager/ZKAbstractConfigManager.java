package com.maolei.zk.config.manager;

import org.I0Itec.zkclient.ZkClient;

import com.maolei.zk.config.ZKUtil;
import com.maolei.zk.config.bean.ZKConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ZKAbstractConfigManager implements ZKConfigManager {
	private ZKConfig config;

	@Override
	public void loadConfigFromDB() {
		this.config = loadFromDB();
		log.info("loadConfigFromDB ==> {}", config);
	}

	@Override
	public void updateConfigToDB(ZKConfig zkConfig) {
		ZkClient zk = ZKUtil.getZkClient();
		config = zk.readData(ZKUtil.MYSQL_CONFIG_NODE_NAME);

		config.setHost(zkConfig.getHost());
		log.info("updateConfigToDB ==> {}", config);
	}

	@Override
	public void syncConfigToZk(String path) {
		log.info("syncConfigToZk ==> {}", config);
		ZkClient zk = ZKUtil.getZkClient();
		if (!zk.exists(path)) {
			zk.createPersistent(path, true);
		}
		zk.writeData(path, config);
		zk.close();
	}

	protected abstract ZKConfig loadFromDB();

}
